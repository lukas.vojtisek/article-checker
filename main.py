import requests
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from urllib.parse import urlparse
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import time
import tqdm
import random
from datetime import datetime

# read the csv file
df = pd.read_csv("1000_articles.csv")

# initialize a progress bar
pbar = tqdm.tqdm(total=len(df))

def check_url(row):
    start_time = time.time()

    try:
        response = requests.get(row['Url'], timeout=10)

        # Check for redirects
        if response.history:
            row['Note'] = "redirect"
            row['Is_available'] = False
        elif "paywall" in response.text or "login" in response.text:  # this is a simplistic check, adjust as needed
            row['Note'] = "paywall / login page"
            row['Is_available'] = False
        elif response.status_code == 200:
            page_content = response.text
            # Compare page content and Text_content using cosine similarity
            vectorizer = TfidfVectorizer().fit_transform([page_content, row['Text_content']])
            similarity = cosine_similarity(vectorizer[0:1], vectorizer[1:2])[0][0]
            row['Similarity'] = similarity
            if similarity < 0.8:  # article has been edited
                row['Note'] = "content edited"
            elif similarity < 0.3:  # article is completely different
                row['Note'] = "content mismatch"
            row['Is_available'] = True
        else:
            row['Is_available'] = False
    except requests.exceptions.RequestException:
        row['Is_available'] = False

    pbar.update()
    pbar.set_postfix({'Processing speed': f'{1 / (time.time() - start_time):.2f} articles/second', 'Estimated time left': f'{(len(df) - pbar.n) * (time.time() - start_time) / 60:.2f} minutes'})

    return row

# create a thread pool and apply the function to each row in the dataframe
with ThreadPoolExecutor(max_workers=40) as executor:  # increase the number of workers
    result = list(executor.map(check_url, [row for _, row in df.iterrows()]))

pbar.close()

# convert the result back to a dataframe and write the results to a new csv file
df_result = pd.DataFrame(result)

# Now let's check the failed ones on archive.org
failed_articles = df_result[df_result['Is_available'] == False]

# Initialize a progress bar for archive.org checks
pbar = tqdm.tqdm(total=len(failed_articles))

for index, row in failed_articles.iterrows():
    archive_url = 'http://archive.org/wayback/available?url=' + row['Url']
    try:
        response = requests.get(archive_url, timeout=10)
        # Generic rate limit check
        while response.status_code == 429:  # HTTP 429 Too Many Requests
            wait_time = random.randint(10, 20)  # random wait time between 10 and 20 seconds
            print(f"Rate limit exceeded for URL: {archive_url}, waiting for {wait_time} seconds before retrying")
            time.sleep(wait_time)
            response = requests.get(archive_url, timeout=10)
        if response.status_code == 200:
            row['Archive_url'] = archive_url
        else:
            row['Archive_url'] = 'Not available'
    except requests.exceptions.RequestException:
        row['Archive_url'] = 'Not available'
    pbar.update()

pbar.close()

df_result.to_csv(f"{datetime.now().strftime('%H-%d-%m-%Y')}.csv", index=False)
